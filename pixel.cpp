/*
Build with
g++ pixel.cpp -w -lSDL2 -o pixel
*/

// #include "sdl.h"

#include <stdlib.h>
#if defined(_MSC_VER)
#include "SDL.h"
#else
#include "SDL2/SDL.h"
#endif

#include <assert.h>

// TB SDL2 migrate
SDL_Window *sdlWindow;
SDL_Renderer *sdlRenderer;
SDL_Surface *screen;

struct Pixel {
	int x, y;
	unsigned int color;
};

struct MovingPixel {
	Pixel start, goal;	
};

static const int kMaxPixels = 100000;
static MovingPixel moving_pixels[kMaxPixels];
static int width=1024;
static int height=768;
int num_frames = 0;

#define LERP(start,end,pct) (int)((start) + (((end) - (start)) * (pct)))
//#define LERP_COLOR(start,end,pct) (LERP(start&0xff,end&0xff,pct)|LERP(start&0xff00,end&0xff00,pct)|LERP(start&0xff0000,end&0xff0000,pct))
/*
#define LERP_COLOR(start,end,pct) ( LERP( (start&0xff0000)>>16, (end&0xff0000)>>16, pct) << 16 |
									LERP( (start&0x00ff00)>>8, (end&0x00ff00)>>8, pct) << 8 |
									LERP( (start&0x0000ff), (end&0x0000ff), pct) )
									*/

// TB TODO - This macro is flickering the colors for some reason.
//#define LERP_COLOR(start,end,pct) ( (LERP( ((start)&0xff0000)>>16, ((end)&0xff0000)>>16, (pct)) << 16) | (LERP( ((start)&0x00ff00)>>8, ((end)&0x00ff00)>>8, (pct)) << 8) | (LERP( ((start)&0x0000ff), ((end)&0x0000ff), (pct))))

unsigned int LERP_COLOR(unsigned int start, unsigned int end, float pct)
{	
	int r1 = (start&0xff0000) >> 16;
	int r2 = (end&0xff0000) >> 16;	
	int r = LERP( r1, r2, pct );

	int g1 = (start&0x00ff00)>>8;
	int g2 = (end&0x00ff00)>>8;
	int g = LERP( g1, g2, pct);

	int b1 = (start&0xff);
	int b2 = (end&0xff);
	int b = LERP( b1, b2, pct );

	assert(r >= 0 && r < 256);

	return (r << 16) | (g << 8) | b;
}


void InitPixels()
{
	//RAND_MAX = 0x7fff;
	/*
	for( int i = 0; i < kMaxPixels; i++ ) {
		Pixel *p = pixels+i;
		p->start_x = rand() % width;
		p->start_y = rand() % height;
		p->start_color = (rand() << 15) | (rand());
		
		//p->start_color = 0xffff0000;
		p->goal_x = rand() % width;
		p->goal_y = rand() % height;
		p->goal_color = (rand() << 15) | (rand());
	}
	*/
	

	SDL_Surface *pImg1, *pImg2;
	SDL_Surface *temp;
	
	//temp = SDL_LoadBMP("hello.bmp");	
	//temp = SDL_LoadBMP("foo1.bmp");
	//temp = SDL_LoadBMP("cat.bmp");
	temp = SDL_LoadBMP("cat2.bmp");
	pImg1 = SDL_ConvertSurface(temp, screen->format, SDL_SWSURFACE);
	SDL_FreeSurface(temp);

	//temp = SDL_LoadBMP("world.bmp");	
	//temp = SDL_LoadBMP("foo2.bmp");	
	temp = SDL_LoadBMP("cat.bmp");
	pImg2 = SDL_ConvertSurface(temp, screen->format, SDL_SWSURFACE);
	SDL_FreeSurface(temp);

	int pixel_num = 0;
	for( int y = 0; y < height; y++ ) {
		int yoff = y * (screen->pitch >> 2);
		for( int x = 0; x < width; x++ ) {
			unsigned int c = ((unsigned int *)pImg1->pixels)[yoff + x];
			if( c != 0 ) {
				MovingPixel *p = moving_pixels + pixel_num;
				p->start.x = x;
				p->start.y = y;
				p->start.color = c;

				pixel_num++;

				if( pixel_num == 100000 ) {
					x = width;
					y = height;
				}
			}
		}
	}

	pixel_num = 0;
	for( int y = 0; y < height; y++ ) {
		int yoff = y * (screen->pitch >> 2);
		for( int x = 0; x < width; x++ ) {
			unsigned int c = ((unsigned int *)pImg2->pixels)[yoff + x];
			if( c != 0 ) {
				MovingPixel *p = moving_pixels + pixel_num;

				p->goal.x = x;
				p->goal.y = y;
				p->goal.color = c;

				pixel_num++;

				if( pixel_num == 100000 ) {
					x = width;
					y = height;
				}
			}
		}
	}

	// Shuffle goal pixels
	
	for( int i = 0; i < pixel_num; i++ ) {
		int a = rand() % pixel_num;		
		Pixel temp;
		Pixel *pFrom = &moving_pixels[i].goal;
		Pixel *pTo = &moving_pixels[a].goal;		
		memcpy( &temp, pTo, sizeof(Pixel) );
		memcpy( pTo, pFrom, sizeof(Pixel) );
		memcpy( pFrom, &temp, sizeof(Pixel) );		
	}
	

	// Set goal to nearest neighbor
	/*
	for( int i = 0; i < pixel_num; i++ ) {

		Pixel *pStart = &moving_pixels[i].start;

		int best_pixel = 0;
		int best_distance = INT_MAX;

		for( int d = 0; d < pixel_num; d++ ) {			
			Pixel *pGoal = &moving_pixels[d].goal;
			int dist = (pStart->x - pGoal->x)*(pStart->x - pGoal->x) + (pStart->y - pGoal->y)*(pStart->y - pGoal->y);
			// TB TODO - Also check that you're not screwing up the previous pixel.
			if( dist < best_distance ) {

				Pixel *pOldGoal = &moving_pixels[i].goal;
				int previous_dist = (pStart->x - pOldGoal->x)*(pStart->x - pOldGoal->x) + (pStart->y - pOldGoal->y)*(pStart->y - pOldGoal->y);
				if( dist < previous_dist ) {
					best_pixel = d;
					best_distance = dist;
				}
			}
		}
		
		if( best_pixel != i ) {
			Pixel temp;
			Pixel *pFrom = &moving_pixels[i].goal;
			Pixel *pTo = &moving_pixels[best_pixel].goal;		
			memcpy( &temp, pTo, sizeof(Pixel) );
			memcpy( pTo, pFrom, sizeof(Pixel) );
			memcpy( pFrom, &temp, sizeof(Pixel) );		
		}
	}
	*/

}

void Render()
{
	/// Lock surface if needed
	if (SDL_MUSTLOCK(screen)) {
		if (SDL_LockSurface(screen) < 0) {
			return;
		}
	}

	// Ask SDL for the time in milliseconds
	int tick = SDL_GetTicks();

	float pct;	
	if( tick % 3000 < 500 ) {
		pct = 0;
	}
	else if( tick % 3000 < 1500 ) {
		pct = (float)((tick+500) % 1000) / 1000.0f;
	}
	else if( tick % 3000 < 2000 ) {
		pct = 1.0f;
	}
	else {
		pct = (float)((tick) % 1000) / 1000.0f;		
		pct = 1.0f - pct;		
	}

	/*
	float pct = (float)(tick % 1000) / 1000.0f;
	if( tick % 2000 > 1000 ) {
		pct = 1.0f - pct;
	}
	*/

	pct = (pct > 1.0f) ? 1.0f : pct;
    pct = (pct < 0.0f) ? 0.0f : pct;

    // Cubicly adjust the amount value.
    pct = (pct * pct) * (3.0f - (2.0f * pct));
    
	

	// Try fading the whole image
	//memset(screen->pixels, 0, screen->pitch * height);
	
	// Fade out the whole image
	for( int y = 0; y < height; y++ ) {
		int yoff = y * (screen->pitch >> 2);
		for( int x = 0; x < width; x++ ) {
			unsigned int *p = &((unsigned int*)screen->pixels)[x + (y * (screen->pitch>>2))];
			if( *p != 0 ) {
				*p = (int) LERP_COLOR(*p, 0, 0.08f);
			}			
		}
	}
	



	for( int i = 0; i < kMaxPixels; i++ ) {

		MovingPixel *p = moving_pixels+i;

		int pos_x = (int) LERP(p->start.x, p->goal.x, pct);
		int pos_y = (int) LERP(p->start.y, p->goal.y, pct);

		unsigned int color = (int) LERP_COLOR(p->start.color, p->goal.color, pct);
		//color = p->start.color;
		//unsigned int color = p->goal.color;
		//unsigned int r = 0xff0000;
		//unsigned int color = #define LERP(start,end,pct) (int)((start) + (((end) - (start)) * (pct)))

		
		// TB TEST - try moving the pixels at different times
		// Looks like crap
		/*
		if( i%1000 < tick%1000 ) {
			pos_x = p->start.x;
			pos_y = p->start.y;
			color = p->start.color;
		}
		*/




		//((unsigned int*)screen->pixels)[p->start_x + (p->start_y * (screen->pitch>>2))] = p->start_color;
		((unsigned int*)screen->pixels)[pos_x + (pos_y * (screen->pitch>>2))] = color;

		// TB TEMP TEST - fat pixels
		/*
		((unsigned int*)screen->pixels)[pos_x+1 + (pos_y * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+2 + (pos_y * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+3 + (pos_y * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+4 + (pos_y * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+0 + ((pos_y+1) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+1 + ((pos_y+1) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+2 + ((pos_y+1) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+3 + ((pos_y+1) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+4 + ((pos_y+1) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+0 + ((pos_y+2) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+1 + ((pos_y+2) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+2 + ((pos_y+2) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+3 + ((pos_y+2) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+4 + ((pos_y+2) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+0 + ((pos_y+3) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+1 + ((pos_y+3) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+2 + ((pos_y+3) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+3 + ((pos_y+3) * (screen->pitch>>2))] = color;
		((unsigned int*)screen->pixels)[pos_x+4 + ((pos_y+3) * (screen->pitch>>2))] = color;
		*/
		
	}
	

	/*
	// Declare a couple of variables
	int i, j, yofs, ofs;
	// Draw to screen
	yofs = 0;
	for (i = 0; i < 480; i++) {
		for (j = 0, ofs = yofs; j < 640; j++, ofs++) {
			((unsigned int*)screen->pixels)[ofs] = i * i + j * j + tick;
		}
		yofs += screen->pitch / 4;
	}
	*/

	// Unlock if needed
	if(SDL_MUSTLOCK(screen)) {
		SDL_UnlockSurface(screen);
	}

	// Tell SDL to update the whole screen
	// TB SDL2 migrate
	//SDL_Flip(screen);
	SDL_RenderPresent(sdlRenderer);

	num_frames++;
}

// Entry point
int main(int argc, char *argv[])
{
	// Initialize SDL's subsystems - in this case, only video.
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) 
	{
		fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
		exit(1);
	}

	// TB SDL2 migrate
	SDL_CreateWindowAndRenderer(640, 480, SDL_WINDOW_FULLSCREEN_DESKTOP, &sdlWindow, &sdlRenderer);

	// Register SDL_Quit to be called at exit; makes sure things are
	// cleaned up when we quit.
	atexit(SDL_Quit);
    
	// Attempt to create a 640x480 window with 32bit pixels.
	//screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE);
	/*
	screen = SDL_SetVideoMode(width, height, 32, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN );
  
	// If we fail, return error.
	if ( screen == NULL ) {
		fprintf(stderr, "Unable to set 640x480 video: %s\n", SDL_GetError());
		exit(1);
	}
	*/

	InitPixels();

	memset(screen->pixels, 0, screen->pitch * height);
 
	// Main loop: loop forever.
	while (1) {
		// Render stuff
		Render();

		// Poll for events, and handle the ones we care about.
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_KEYDOWN:
				break;
			case SDL_KEYUP:
				// If escape is pressed, return (and thus, quit)
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					printf("Num frames: %d, FPS: %d", num_frames, num_frames/SDL_GetTicks()/1000);
					return 0;
				}
				break;
			case SDL_QUIT:				
				return(0);
			}
		}
	}
	return 0;
}
